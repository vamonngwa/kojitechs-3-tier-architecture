
variable "vpc_cidr" {
  description = "vpc_cidr"
  type        = string
  default     = "10.0.0.0/16"
}

variable "public_cidr" {
  description = "cidr for public subnet"
  type        = list(any)
  default     = ["10.0.0.0/24", "10.0.2.0/24"]
}

variable "private_cidr" {
  description = "cidr for private subnet"
  type        = list(any)
  default     = ["10.0.1.0/24", "10.0.3.0/24"]
}

variable "database_cidr" {
  description = "cidr for database subnet"
  type        = list(any)
  default     = ["10.0.5.0/24", "10.0.7.0/24"]
}

variable "component_name" {
  default = "kojitech-register"
}


variable "tag_name" {
  type        = list(any)
  description = "describe the variable "
  default     = ["RegistrationAPP_A", "RegistrationAPP_B"]
}